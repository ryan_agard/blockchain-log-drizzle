import React, { useState, useEffect } from "react";
import levelUpLogo from "../levelupLogo.png";
import * as Promise from "bluebird";

export default ({ drizzle, drizzleState }) => {

  const contract = drizzle.contracts.TimelogToken;
  const accounts = drizzleState.accounts;
  console.log('drizzleState: ', drizzleState);
  console.log('drizzle: ', drizzle);

  const [ticketData, setTicketNumbers] = useState([]);
  const [totalSupply, setTotalSupply] = useState(0);
  const [allowanceState, setAllowance] = useState(0);
  const [balance, setBalance] = useState(null);
  const [receipts, setReciepts] = useState([]);

  useEffect(() => {
    getTotalSupply();
    getTickets();
    getTokenOwner();
    getJiraMappings();
  }, []);

  const getTicketAddresses = async (tickets) => {
    return new Promise((resolve, reject) => {
      Promise.map(tickets, (ticket) => {
        return new Promise((resolveMap, rejectMap) => {
          return contract.methods.getAddressByTicketNum(parseInt(ticket)).call().then((address) => {
            return resolveMap({ address, ticket });
          }).catch((err) => {
            return rejectMap(err);
          })
        })
      }, {
        concurrency: 1,
      }).then((ticketDatas) => {
        return resolve(ticketDatas);
      }).catch((err) => {
        return reject(err);
      })
    })
  }

  const getTickets = async () => {

    const ticketsRaw = await contract.methods.getTickets().call();
    console.log('ticketsRaw: ', ticketsRaw);
    const tickets = ticketsRaw.filter((ticketNumber) => ticketNumber !== '0');
    console.log('tickets: ', tickets);

    const ticketData = await getTicketAddresses(tickets);
    console.log("ticketData: ", ticketData);
    setTicketNumbers(ticketData);
  }

  const getTokenOwner = async () => {
    const owner = await contract.methods.tokenOwner().call();
    console.log('owner: ', owner);
  }

  const getAllowance = async (index) => {
    const allowance = await contract.methods.allowance(accounts[0], accounts[index]).call();
    setAllowance(allowance);
    console.log('allowance: ', allowance);
  }

  const approveDelegate = async (index) => {
    const approveResponse = await contract.methods.approve(accounts[index], 500).send();
    console.log('approveResponse: ', approveResponse);
  }

  const distributeTokens = async (index) => {
    const response = await contract.methods.transfer(accounts[index], 500).send();
    console.log('response: ', response);
  }

  const getTotalSupply = async () => {
    const totalSupply = await contract.methods.totalSupply().call();
    console.log('totalSupply: ', totalSupply);
    setTotalSupply(totalSupply);
  }

  const getBalanceOf = async (index) => {
    const balance = await contract.methods.balanceOf(accounts[index]).call();
    console.log('balance: ', balance);
    setBalance({ balance, index });
  }

  const getJiraMappings = async () => {
    const res = await contract.methods.getAddressByTicketNum(3615).call();
    console.log('res: ', res);
  }

  const addTicket = async (ticketNum, index) => {
    const receipt = await contract.methods.addJiraTicket(ticketNum, accounts[index]).send();
    console.log('receipt: ', receipt);
    getTickets();
  }

  const logHours = async (ticket, hours, userAddress) => {
    const ticketAddress = ticketData.filter((ticketObj) => ticketObj.ticket === ticket);
    if(ticketAddress.length) {
      // we must approve userAddress to send tokens to ticketAddress
      const approveResponse = await contract.methods.approve(ticketAddress[0].address, hours).send({ from: userAddress });
      console.log('approveResponse: ', approveResponse);

      // checking allowance of tokens to be transferred from userAddress to ticketAddress
      const allowance = await contract.methods.allowance(userAddress, ticketAddress[0].address).call();
      console.log('allowance: ', allowance);

      // transferring tokens from userAddress to ticketAddress
      const receipt = await contract.methods.transferFrom(userAddress, ticketAddress[0].address, hours).send({ from: ticketAddress[0].address });
      const receiptsCopy = receipts;
      receiptsCopy.push(receipt);
      setReciepts(receiptsCopy);
      console.log('receipt: ', receipt);
    } else {
      console.log('invalid ticket number');
    }
  }

  const getTicketIndex = (address) => {
    for (let key in accounts) {
      if (accounts[key] === address) {
        return key;
      }
    }
    return '';
  }

  return (
    <div className={"main"}>
      <div className={"logoContainer"}>
        <img src={levelUpLogo} alt="level-up-logo" />
      </div>
      <h1>TimeLog Token</h1>
      <button onClick={() => {
        console.log('receipts: ', receipts);
      }}>log</button>

      <div className={"sectionContainer"}>
        <h3>All Added Jira Tickets</h3>
        {ticketData.length ? (
          <div style={{ width: '100%' }}>
            {ticketData.map((data, i) => {
              if (data.address && data.ticket) {
                return (
                  <div key={i} style={{ width: '77%', display: 'flex', justifyContent: 'space-between', paddingLeft: '40px' }}>
                    <p>{`Ticket Index: ${getTicketIndex(data.address)}`}</p>
                    <p>{`Ticket #: ${data.ticket}`}</p>
                  </div>
                )
              }
            })}
          </div>
        ) : null}
      </div>

      <div className={"sectionContainer"}>
        <h2>Log Hours</h2>
        <div className={"logHoursInputContainer"}>
          <div className={"logHoursInputWrapper"}>
            <p>Ticket #</p>
            <input type="text" id="ticketNumber" className={"inputField"}></input>
          </div>
          <div className={"logHoursInputWrapper"}>
            <p>Hours</p>
            <input type="text" id="numOfHours" className={"inputField"}></input>
          </div>
          <div className={"logHoursInputWrapper"}>
            <p>User ID</p>
            <input type="text" id="userId" className={"inputField"}></input>
          </div>
          <div style={{ width: '375px', marginLeft: '-40px' }}>
            <p>{"For now, Address ID field must be a number between 1 and 5.  These numbers respresent an account index for an ethereum address"}</p>
          </div>
          <div className={"logHoursBtnContainer"}>
            <button
              style={{ width: '75px' }}
              onClick={() => {
                const inp1 = document.getElementById('ticketNumber').value;
                const inp2 = parseInt(document.getElementById('numOfHours').value);
                const inp3 = parseInt(document.getElementById('userId').value);
                logHours(inp1, inp2, accounts[inp3])
              }}>Submit</button>
          </div>
        </div>
      </div>

      <div className={"sectionContainer"}>
        <h3>Add New Jira Ticket</h3>
        <div style={{ padding: '0px 30px 0px 30px' }}>
          <p>{"Enter an address index between 6 and 9.  You can't use an index that's already displayed in the 'All Added Jira Tickets' section below"}</p>
        </div>
        <div className={"logHoursInputWrapper"}>
          <p>Jira Ticket #</p>
          <input type="text" id="addTicketNumber" className={"inputField"}></input>
        </div>

        <div className={"logHoursInputWrapper"}>
          <p>Address ID</p>
          <input type="text" id="addTicketAddress" className={"inputField"}></input>
        </div>

        <div className={"logHoursBtnContainer"}>
          <button
            style={{ width: '75px' }}
            onClick={() => {
              const ticketNum = parseInt(document.getElementById('addTicketNumber').value);
              const accountIndex = parseInt(document.getElementById('addTicketAddress').value);
              addTicket(ticketNum, accountIndex);
            }}>Submit</button>
        </div>
      </div>

      {/* <div className={"sectionContainer"}>
        <h3>Approve account for transfer</h3>
        <div style={{ padding: '0px 100px 0px 110px' }}>
          <p>{"Enter account index to be approved for 1000 tokens"}</p>
        </div>
        <div className={"allowanceBtnContainer"}>
          <input type="text" id="approve" style={{ width: '97px' }}></input>
          <button
            style={{ width: '105px' }}
            onClick={() => {
              const delegateIndex = parseInt(document.getElementById('approve').value);
              approveDelegate(delegateIndex);
            }}>approve</button>
        </div>
      </div> */}

      <div className={"sectionContainer"}>
        <h3>Distribute tokens to account</h3>
        <div className={"logHoursInputContainer"}>
          <div className={"logHoursInputWrapper"}>
            <p>Account ID</p>
            <input type="text" id="distributeAddress" className={"inputField"}></input>
          </div>
          <div className={"logHoursBtnContainer"}>
            <button style={{ marginTop: '10px' }} onClick={() => {
              const addr = document.getElementById('distributeAddress').value;
              distributeTokens(addr);
            }}>{'Distribute'}</button>
          </div>
        </div>
      </div>

      <div className={"sectionContainer"}>
        <h3>Check balance of account by index</h3>
        <div className={"allowanceBtnContainer"}>
          <input type="text" id="accountIndex" style={{ width: '97px' }}></input>
          <button
            style={{ width: '105px' }}
            onClick={() => {
              const accountIndex = parseInt(document.getElementById('accountIndex').value);
              getBalanceOf(accountIndex);
            }}>balanceOf</button>
        </div>
        {balance ? (
            <div style={{ marginTop: '10px' }}>
              <p>{`Balance of Account # ${balance.index}: `}<span style={{ fontWeight: "bold" }}>{balance.balance}</span></p>
            </div>
          ) : null}
      </div>

      <div className={"sectionContainer"}>
        <h3>Check Allowance</h3>
        <div className={"checkAllowanceBody"}>
          <div style={{ padding: "10px" }}>
            <p>{"Enter an index 1 - 5 to check that account's allowance. Allowance refers to the number of tokens authorized to be transfered to that account"}</p>
            <p>{"If the displayed value is 0, the given account must be approved before tokens are transferred"}</p>
          </div>

        </div>
        <div className={"allowanceBtnContainer"}>
          <input type="text" id="allowance" style={{ width: '97px' }}></input>
          <button
            style={{ width: '105px' }}
            onClick={() => {
              const allowanceVal = parseInt(document.getElementById('allowance').value);
              getAllowance(allowanceVal);
            }}>get allowance</button>

        </div>
        {allowanceState ? (
          <div>
            <p>{"Allowance: "}<span style={{ fontWeight: "bold" }}>{allowanceState}</span></p>
          </div>
        ) : null}
      </div>

      <div className={"sectionContainer"} style={{ marginBottom: '50px' }}>
        <h3>Total Supply</h3>
        <div>
          <p>{"Total Supply of Timelog Token: "}<span style={{ fontWeight: "bold", paddingLeft: '3px' }}>{totalSupply}</span></p>
        </div>
      </div>
    </div>
  );
};
