import Web3 from "web3";
import TimelogToken from "./contracts/TimelogToken.json";

const options = {
  web3: {
    block: false,
    customProvider: new Web3("ws://localhost:8545"),
  },
  contracts: [TimelogToken],
  events: {
    SimpleStorage: ["StorageSet"],
  },
};

export default options;
