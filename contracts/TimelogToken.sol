// SPDX-License-Identifier: MIT
pragma solidity >=0.4.21 <0.7.0;
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract TimelogToken is ERC20 {
    string public name = "TimelogToken";
    string public symbol = "TT";
    uint256 public decimals = 2;
    uint256 public INITIAL_SUPPLY = 12000;
    uint256 totalSupply_;
    address public tokenOwner;

    // mapping(address => uint256) balances;
    // mapping(address => mapping (address => uint256)) allowed;

    mapping (uint => address) jira_mappings;
    mapping(address => uint256) balances;
    mapping(address => mapping (address => uint256)) allowed;

    // uint256[] public ticketNumbers = new uint256[](1000);
    uint[] public ticketNumbers = new uint[](1000);

    constructor() public {
        _mint(msg.sender, INITIAL_SUPPLY);
        tokenOwner = msg.sender;
        totalSupply_ = INITIAL_SUPPLY;
        approve(msg.sender, totalSupply_);
        // balances[msg.sender] = INITIAL_SUPPLY;
        // allowed[msg.sender][msg.sender] = 12000;
    }

    function addJiraTicket(uint ticket_number, address _address) public returns (uint) {
        // uint[] storage tempTicketNumbers = ticketNumbers;
        jira_mappings[ticket_number] = _address;
        ticketNumbers.push(ticket_number);
        // ticketNumbers = tempTicketNumbers;
        return ticket_number;
    }

    function getTickets() external view returns (uint[] memory) {
        return ticketNumbers;
    }

    function getAddressByTicketNum(uint ticketNum) public view returns (address) {
        return jira_mappings[ticketNum];
    }

    // function logHours(uint256 ticket_number, uint256 numOfHours, address userAddress) public returns (bool) {
    //     address ticketAddress = jira_mappings[ticket_number];
    //     return transferFrom(userAddress, ticketAddress, numOfHours);
    // }

    // function transferFrom(address sender, address recipient, uint256 amount) public returns (bool) {
    //     _transfer(sender, recipient, amount);
    //     _approve(sender, _msgSender(), _allowances[sender][_msgSender()].sub(amount, "ERC20: transfer amount exceeds allowance"));
    //     return true;
    // }


    // we've got ticketNum > address;
    // address will receive tokens.
    // log hours will send numOfHours to the address mapped to the ticket number
}
